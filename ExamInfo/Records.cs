using System;
using System.Diagnostics.CodeAnalysis;

namespace ExamInfo{
    public record Record{
        public Record(string name, string @class, string exam, int grade)
        {
            Name = name;
            Class = @class;
            Exam = exam;
            Grade = grade;
        }

        public string Name {get;init;}
        public string Class {get;init;}
        public string Exam {get;init;}
        public int Grade {get;init;}

    }

    public class StudentWithAverageGrade {
        public StudentWithAverageGrade(string name, string group, double grade)
        {
            Name = name;
            Group = group;
            Grade = grade;
        }

        public String Name{get;init;}
        public String Group{get;init;}
        public double Grade {get;init;}


        public override bool Equals(object? obj)
        {
            if(this == obj) return true;
            if(obj is not StudentWithAverageGrade) return false;
            var o = (StudentWithAverageGrade) obj;
            return Name == o.Name && Group == o.Group && Math.Abs(Grade - o.Grade) <= 1e-2;
        }

    }

    public class ExamWithAverageGrade{
        
        public ExamWithAverageGrade(string exam, double grade)
        {
            Exam = exam;
            Grade = grade;
        }
        public string Exam{get;init;}
        public double Grade{get;init;}

        public override bool Equals(object? obj)
        {
            if(this == obj) return true;
            if(obj is not ExamWithAverageGrade) return false;
            var o = (ExamWithAverageGrade) obj;
            return Exam == o.Exam && Math.Abs(Grade - o.Grade) <= 1e-2;
        }
    }

    public class GroupWithAverageGradeOfExam {
        public GroupWithAverageGradeOfExam(string group, string exam, double grade)
        {
            Group = group;
            Exam = exam;
            Grade = grade;
        }

        public string Group{get;init;}
        public string Exam{get;init;}

        public double Grade{get;init;}

        public override bool Equals(object? obj)
        {
            if(this == obj) return true;
            if(obj is not GroupWithAverageGradeOfExam) return false;
            var o = (GroupWithAverageGradeOfExam) obj;
            return Exam == o.Exam && Group == o.Group && Math.Abs(Grade - o.Grade) <= 1e-2;
        }
    }
}