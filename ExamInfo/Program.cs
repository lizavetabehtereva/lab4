﻿using System;
using System.Linq;

namespace ExamInfo{


    public class Program{
        static void Main(){return;}

        //Задание 1
        public static IEnumerable<StudentWithAverageGrade>? GetStudentWithMaxAverageGrade(IEnumerable<Record> records){
            if(records is null){
                return null;
            }
            var c1 = (from r in records group r by r.Name into grouped
                     from student in grouped select new StudentWithAverageGrade(student.Name,student.Class,
                     (from s in grouped where s.Name == student.Name select s).Average(s => s.Grade))
                     ).DistinctBy(s => s.Name);
            return from student in c1 where student.Grade == c1.Max(s => s.Grade) select student;
        }

        //Задание 2
        public static IEnumerable<ExamWithAverageGrade>? GetAverageGradeOfExam(IEnumerable<Record> records){
            if(records is null) {
                return null;
            }
            var c1 = from r in records group r by r.Exam into exams 
                     from e in exams select new ExamWithAverageGrade(e.Exam,
                     (from ex in exams where ex.Exam == e.Exam select ex).Average(ex => ex.Grade));
            return c1.DistinctBy(ex => ex.Exam);
        }

        //Задание 3
        public static IEnumerable<GroupWithAverageGradeOfExam>? GetGroupWithMaxAverageGradeOfExam(IEnumerable<Record> records){
            if(records is null) {
                return null;
            }
            var c1 = from r in records group r by r.Class ; //по предмету - записи
            var c2 = from c in c1
                     from r in c group c by r.Exam 
                     ; 

            var c3 = from e in c2 //для c - список предметов  из списка групп
                     from c in e //для e - список записей из списка предметов
                     from r in c // для каждой записи из группа - предмет - запись 
                     select new GroupWithAverageGradeOfExam(c.Key,e.Key,
                     (  from record in c 
                        where record.Exam == e.Key && record.Class == c.Key 
                        select record) 
                     // выбрать из c - список записей по группам по предмету
                     .Average(record => record.Grade)
                     )
                    ;
            var c4 = c3.DistinctBy(el => new{el.Exam,el.Group});
            var c5 = from r in c4 group r by r.Exam;
            var c6 = from e in c5
                     from r in e
                     where r.Grade == e.Max(record => record.Grade)
                     select r;   
            return c6;
        }
    }

    


}
