using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using ExamInfo;
using System.Collections.Generic;

namespace Tests;

[TestClass]
public class GetGroupWithMaxAverageGradeOfExamTests
{
    public static IEnumerable<object[]> GetData(){
        yield return new object[] {
            new List<Record>{
                new("1","1","1",5),
                new("1","1","2",5),

                new("2","2","1",4),
                new("2","2","2",4),
            },
            new List<GroupWithAverageGradeOfExam>{
                new("1","1",5),
                new("1","2",5)

            }
        };
        yield return new object[]{
             new List<Record>{
                new("1","1","1",5),
                new("1","1","2",2),
                new("1","1","3",3),

                new("2","2","1",5),
                new("2","2","2",2),
                new("2","2","3",3),

                new("3","3","1",5),
                new("3","3","2",2),
                new("3","3","3",3),
            },
            new List<GroupWithAverageGradeOfExam>{
                new("1","1",5),
                new("2","1",5),
                new("3","1",5),

                new("1","2",2),
                new("2","2",2),
                new("3","2",2),

                new("1","3",3),
                new("2","3",3),
                new("3","3",3),

            }
        };
         yield return new object[] {
            new List<Record>{
                new("1","1","1",2),
                new("1","1","2",5),

                new("1","1","1",2),
                new("1","1","2",4),

                new("2","2","1",4),
                new("2","2","2",4),

                new("2","2","1",3),
                new("2","2","2",2),
            },
            new List<GroupWithAverageGradeOfExam>{
                new("2","1",3.5),
                new("1","2",4.5)

            }
        };
    }
    
    [TestMethod]
    [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
    public void GetGroupWithMaxAverageGradeOfExam_HasResults(IEnumerable<Record> records,IEnumerable<GroupWithAverageGradeOfExam> expected)
    {
        var actual = Program.GetGroupWithMaxAverageGradeOfExam(records);

        CollectionAssert.AreEqual(actual.ToList(),expected.ToList());
    }

    [TestMethod]
    public void GetGroupWithMaxAverageGradeOfExam_HasResultsDebug()
    {
        var actual = Program.GetGroupWithMaxAverageGradeOfExam(  new List<Record>{
                new("1","1","1",5),
                new("1","1","2",5),

                new("2","2","1",4),
                new("2","2","2",4),
            });

        CollectionAssert.AreEqual(actual.ToList(), new List<GroupWithAverageGradeOfExam>{
                new("1","1",5),
                new("1","2",5)

            });
    }

    [TestMethod]
    public void GetGroupWithMaxAverageGradeOfExam_CanInputNull(){
        try{
            Program.GetGroupWithMaxAverageGradeOfExam(null);
        }
        catch(Exception exception){
            Assert.Fail("Метод не должен выкидывать исключения");
        }
    }
}