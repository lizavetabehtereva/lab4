using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using ExamInfo;
using System.Collections.Generic;

namespace Tests;

[TestClass]
public class GetAverageGradeOfExamTests
{
    public static IEnumerable<object[]> GetData(){
        yield return new object[] {
            new List<Record>{
                new("1","1","1",5),
                new("1","1","2",5),

                new("2","2","1",4),
                new("2","2","2",4),
            },
            new List<ExamWithAverageGrade>{
                new("1",4.5),
                new("2",4.5)
            }
        };
        yield return new object[]{
             new List<Record>{
                new("1","1","1",5),
                new("2","2","1",2),
                new("3","3","1",3),
            },
            new List<ExamWithAverageGrade>{
                new("1",3.333),
            }
        };
    }
    
    [TestMethod]
    [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
    public void GetAverageGradeOfExam_HasResults(IEnumerable<Record> records,IEnumerable<ExamWithAverageGrade> expected)
    {
        var actual = Program.GetAverageGradeOfExam(records);

        CollectionAssert.AreEqual(actual.ToList(),expected.ToList());
    }

    [TestMethod]
    public void GetAverageGradeOfExam_HasResultsDebug()
    {
        var actual = Program.GetAverageGradeOfExam(new List<Record>{
                new("1","1","1",5),
                new("2","2","1",2),
                new("3","3","1",3),
            });

        CollectionAssert.AreEqual(actual.ToList(),  new List<ExamWithAverageGrade>{
                new("1",3.333),
            });
    }

    [TestMethod]
    public void GetAverageGradeOfExam_CanInputNull(){
        try{
            Program.GetAverageGradeOfExam(null);
        }
        catch(Exception exception){
            Assert.Fail("Метод не должен выкидывать исключения");
        }
    }
}