using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using ExamInfo;
using System.Collections.Generic;

namespace Tests;

[TestClass]
public class MaxAverageStudentTests
{
    public static IEnumerable<object[]> GetData(){
        yield return new object[] {
            new List<Record>{
                new("1","1","1",5),
                new("1","1","2",5),

                new("2","2","1",4),
                new("2","2","2",4),
            },
            new List<StudentWithAverageGrade>{
                new("1","1",5)
            }
        };
        yield return new object[]{
             new List<Record>{
                new("1","1","1",5),
                new("1","1","2",2),
                new("1","1","3",3),
            },
            new List<StudentWithAverageGrade>{
                new("1","1",3.33)
            }
        };
    }

    [TestMethod]
    [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
    public void GetStudentWithMaxAverageGrade_HasResults(IEnumerable<Record> records,IEnumerable<StudentWithAverageGrade> expected)
    {
        var actual = Program.GetStudentWithMaxAverageGrade(records);

        CollectionAssert.AreEqual(actual.ToList(),expected.ToList());
    }

    [TestMethod]
    //По какой-то причине DynamicData отказывается дебажится
    public void GetStudentWithMaxAverageGrade_HasResults_Debug()
    {
        var actual = Program.GetStudentWithMaxAverageGrade(new List<Record>{
                new("1","1","1",5),
                new("1","1","2",2),
                new("1","1","3",3),
            });

        CollectionAssert.AreEqual(actual.ToList(),new List<StudentWithAverageGrade>{
                new("1","1",3.33)
            });
    }

    [TestMethod]
    public void GetStudentWithMaxAverageGrade_CanInputNull(){
        try{
            Program.GetStudentWithMaxAverageGrade(null);
        }
        catch(Exception exception){
            Assert.Fail("Метод не должен выкидывать исключения");
        }
    }
}